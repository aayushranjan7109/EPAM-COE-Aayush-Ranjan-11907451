import React from 'react';

const DownloadButton = ({ onClick }) => {
  return (
    <button onClick={onClick}>
      Download Grades
    </button>
  );
};

export default DownloadButton;
