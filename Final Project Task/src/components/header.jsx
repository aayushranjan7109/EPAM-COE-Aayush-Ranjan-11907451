import React from 'react';
import './header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Header() {
  const today = new Date().toLocaleDateString();
  const professorName = 'Aayush Ranjan';
  const college = 'LPU';
  const department = 'CSE';
  const semester = 'Final Semester';

  return (
    <header>
      <div className="header-left">
        <FontAwesomeIcon icon="fa-solid fa-book-open" beat size="2xl" style={{color: "#511f2e"}} />
        <h1>Grade Book</h1>
      </div>
      <div className="header-centre">
      <p><FontAwesomeIcon icon="fa-solid fa-user-tie" size="xl" /> Professor: {professorName}</p>
        <p><FontAwesomeIcon icon="fa-solid fa-calendar-days" size="xl" />  Today's Date: {today}</p>
      </div>
      <div className="header-left">
        
        <p><FontAwesomeIcon icon="fa-solid fa-building-columns" size="xl" /> College: {college}</p>
        <p><FontAwesomeIcon icon="fa-solid fa-building-user" size="xl" /> Department: {department}</p>
        <p><FontAwesomeIcon icon="fa-solid fa-star" size="xl" /> Semester: {semester}</p>
      </div>
    </header>
  );
}

export default Header;
