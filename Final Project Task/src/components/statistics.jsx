import React from "react";
import { PieChart } from "react-minimal-pie-chart";
import students from '../studentgrade';

const PieChartComponent = () => {
  const data = [
    { title: "Pass", value: students.filter(s => s.overallGrade >= 4).length, color: "#4CAF50" },
    { title: "Fail", value: students.filter(s => s.overallGrade < 4).length, color: "#F44336" },
  ];

  return (
    <PieChart
      data={data}
      lineWidth={60}
      paddingAngle={5}
      label={({ dataEntry }) => `${dataEntry.title}: ${dataEntry.value}`}
      labelStyle={{ fontSize: "20px", fontFamily: "sans-serif", fontWeight: "bold" }}
      animate={true}
      animationDuration={1500}
      animationEasing="ease-out"
      startAngle={-90}
      viewBoxSize={[600, 400]}
      center={[300, 200]}
      radius={150}
      segmentsShift={5}
      segmentsStyle={{ transition: 'stroke .3s', cursor: 'pointer' }}
      reveal={50}
      onClick={(event, segmentIndex) => {
        console.log(`Clicked segment ${segmentIndex}`);
      }}
    />
  );
};

export default PieChartComponent;
