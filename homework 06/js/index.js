/* eslint-disable no-mixed-spaces-and-tabs */
function visitLink(path) {
	//your code goes here
	let count = localStorage.getItem(path) || 0;
	localStorage.setItem(path, ++count);
  }
  function viewResults() {
	//your code goes here
	const result = {};
	Object.entries(localStorage).forEach(([key, value]) => {
	  result[key] = value;
	});
	const resultList = document.createElement('ul');
	for (const [key, value] of Object.entries(result)) {
	  const listItem = document.createElement('li');
	  listItem.innerText = `You visited ${key} ${value} time(s)`;
	  resultList.appendChild(listItem);
	}
	document.getElementById('content').appendChild(resultList);
	// Clear the localStorage
	localStorage.clear();
  }